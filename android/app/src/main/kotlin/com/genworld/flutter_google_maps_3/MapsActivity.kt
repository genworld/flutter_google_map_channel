package com.genworld.flutter_google_maps_3

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.CardView
import android.view.View
import android.widget.TextView

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import android.graphics.Bitmap
import android.graphics.Bitmap.Config
import android.graphics.Canvas
import android.support.v4.content.ContextCompat
import com.google.android.gms.maps.model.BitmapDescriptor



class MapsActivity : AppCompatActivity(), OnMapReadyCallback,
        OnMarkerClickListener {

    private lateinit var mMap: GoogleMap

    override fun onMarkerClick(p0: Marker?): Boolean {

        p0?.setInfoWindowAnchor(-10f, -10f)

        val titleView = findViewById<TextView>(R.id.title)
        val snippetView = findViewById<TextView>(R.id.snippet)

        titleView.text = p0?.title
        snippetView.text = p0?.snippet

        val card_view = findViewById<CardView>(R.id.card_view)
        card_view.visibility = View.VISIBLE

        return false
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap


        addMarkerCustom(
                48.863471,
                2.41529,
                "Novotel",
                "1 avenue de la République\n93170 Bagnolet\nTournée 1",
                    R.mipmap.number_1
                )
        addMarkerCustom(
                48.936405,
                2.357015,
                "SFR",
                "5 place Jean Jaures\n93380 Pierrefittet\nTournée 2",
                R.mipmap.number_2
        )
        addMarkerCustom(
                48.813962,
                2.344879,
                "Air Liquide",
                "28 rue d\'Arcueil\n94250 Gentilly\nTournée 3",
                R.mipmap.number_3
        )
        addMarkerCustom(
                48.897684,
                2.284233,
                "Micropole Wide",
                "91-95 rue Carnot\n92300 Levallois\nTournée 4",
                R.mipmap.number_4
        )

        val paris = LatLng(48.859950, 2.339851)
        val zoomLevel = 11.4746f //This goes up to 21
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(paris, zoomLevel))

        mMap.getUiSettings().setZoomControlsEnabled(true)
        mMap.setOnMarkerClickListener(this)
    }

    private fun addMarkerCustom(lat: Double, lng: Double, title: String, snippet: String, vectorResId: Int){
        val sydney = LatLng(lat, lng)
        mMap.addMarker(MarkerOptions()
                .position(sydney)
                .title(title)
                .snippet(snippet)
                .icon(bitmapDescriptorFromVector(this,vectorResId))
        )
    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(0,  0, vectorDrawable.intrinsicWidth * 4, vectorDrawable.intrinsicHeight * 4)
        val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth * 4, vectorDrawable.intrinsicHeight * 4, Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)


        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

}
