package com.genworld.flutter_google_maps_3

import android.os.Bundle

import io.flutter.app.FlutterActivity
import io.flutter.plugins.GeneratedPluginRegistrant


import android.content.Intent
import android.util.Log
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel

class MainActivity: FlutterActivity() {
  private val CHANNEL = "test_activity"

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    GeneratedPluginRegistrant.registerWith(this)
    Log.i("HomeActivity", "oncreate")
    MethodChannel(flutterView, CHANNEL).setMethodCallHandler(
            object : MethodChannel.MethodCallHandler {
              override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
                if(call.method.equals("startNewActivity")) {
                  startNewActivity()
                }
              }
            })

  }

  private fun startNewActivity() {
    val intent = Intent(this, MapsActivity::class.java)
    startActivity(intent)
  }
}
