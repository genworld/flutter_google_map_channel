import 'package:flutter/material.dart';

import 'package:flutter/services.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.indigo,
      ),
      home: FlutterPage(),
    );
  }
}

class FlutterPage extends StatefulWidget {
  @override
  FlutterComponent createState() => new FlutterComponent();
}

class FlutterComponent extends State<FlutterPage> {
  static const String _channel = 'test_activity';
  static const platform = const MethodChannel(_channel);

  @override
  void initState() {
    super.initState();

    _getNewActivity();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
          itemCount: 1,
          itemBuilder: (context, rowNumber) {
            return new Container(
              padding: EdgeInsets.all(16.0),
              child: new Column(
                children: <Widget>[
                  new Text('Appli Test Air Liquide',
                    style: TextStyle(fontSize: 20.0, color: Colors.blue),
                    textAlign: TextAlign.center,
                  ),
                  new SizedBox(height: 16.0,),
                  new MaterialButton(
                      child: const Text('Lancer la page Google Maps Native sur android'),
                      elevation: 5.0,
                      height: 48.0,
                      minWidth: 250.0,
                      color: Colors.blue,
                      textColor: Colors.white,
                      onPressed: () => _getNewActivity()),
                ],
              ),
            );
          }),
    );
  }

  _getNewActivity() async {
    try {
      await platform.invokeMethod('startNewActivity');
    } on PlatformException catch (e) {
      print(e.message);
    }
  }
}
